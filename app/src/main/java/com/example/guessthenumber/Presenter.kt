package com.example.guessthenumber

import android.util.Log
import kotlin.math.abs

class Presenter {
    val listOfNumber = listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    private lateinit var player1: Player
    private lateinit var player2: Player
    private lateinit var view: MainActivityView

    fun setView(view: MainActivityView){
        this.view = view
    }

    fun setPlayer1(player: Player) {
        player1 = player
    }

    fun setPlayer2(player: Player) {
        player2 = player
    }

    fun randomTheNumber(): Int {
        return listOfNumber.random()
    }

    fun checkTheResult(player1Choice: Int, player2Choice: Int) {
        val number = randomTheNumber()
        player1.playerChoice = player1Choice
        player2.playerChoice = player2Choice
        val player1Diff = abs(player1.playerChoice - number)
        val player2Diff = abs(player2.playerChoice - number)
        Log.d("chechResult", "${player1.playerChoice} ${player2.playerChoice}")
        Log.d("checkTheResult", "$player1Diff $number $player2Diff")
        when {
            player1Diff < player2Diff -> {
                player1.playerScore++
            }
            player2Diff < player1Diff -> {
                player2.playerScore++
            }
            else -> {
                Log.d("checkTheResult","Draw")
            }
        }
        view.setNotification("Please input your next number")
    }

    fun checkTheWinner(){
        Log.d("checkTheWinner", "${player1.playerScore} ${player2.playerScore}")
        if (player1.playerScore > player2.playerScore){
            view.setWinner("${player1.playerName} Win")
        }else if (player1.playerScore < player2.playerScore){
            view.setWinner("${player2.playerName} Win")
        }else{
            view.setWinner("Draw")
        }
    }
}