package com.example.guessthenumber

interface MainActivityView {
    fun setWinner(playerName:String)
    fun setNotification(notification:String)
}