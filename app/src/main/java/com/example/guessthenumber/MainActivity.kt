package com.example.guessthenumber

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainActivityView {
    lateinit var player: Player
    lateinit var computer:Player
    val listOfNumber = listOf(0,1,2,3,4,5,6,7,8,9)
    lateinit var presenter :Presenter
    var round = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = Presenter()
        presenter.setView(this)

        btn_start.setOnClickListener {
            player = Player(tv_player_name.text.toString())
            computer = Player("Computer")
            presenter.setPlayer1(player)
            presenter.setPlayer2(computer)
            round = 1
            btn_start.visibility = View.GONE
            tv_player_name.visibility = View.GONE
            btn_guess.visibility = View.VISIBLE
            tv_player_choice.visibility = View.VISIBLE
        }

        btn_guess.setOnClickListener {
            if(round<3){
                if(tv_player_choice.text.toString().toInt() !in listOfNumber){
                    tv_winner.text = "Please input number between 0-9"
                }else{
                    presenter.checkTheResult(tv_player_choice.text.toString().toInt(),listOfNumber.random())
                }
                round++
            }else if (round == 3){
                if(tv_player_choice.text.toString().toInt() !in listOfNumber){
                    tv_winner.text = "Please input number between 0-9"
                }else{
                    presenter.checkTheResult(tv_player_choice.text.toString().toInt(),listOfNumber.random())
                    presenter.checkTheWinner()
                }
                round++
            }

        }
    }

    override fun setWinner(playerName: String) {
        tv_winner.text = playerName
        btn_guess.visibility = View.GONE
        tv_player_choice.visibility = View.GONE
        btn_start.visibility = View.VISIBLE
        tv_player_name.visibility = View.VISIBLE
    }

    override fun setNotification(notification: String) {
        tv_winner.text = "Round ${round+1}: $notification"
    }
}